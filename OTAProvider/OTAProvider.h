
#ifndef OTAProvider_h
#define OTAProvider_h
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

class OTAProvider{

  private:
    char* ssidWifiOTA;
    char* passwordWifiOTA;
    char* pskArduinoOTA;
    int port;
    char* hostName;
    

  public:
    OTAProvider(char* ssidwifi, char* passwordwifi);
    OTAProvider(char* ssidwifi, char* passwordwifi, char* parampskArduinoOTA);
    OTAProvider(char* ssidwifi, char* passwordwifi, char* parampskArduinoOTA, int paramPort, char* paramHostName);
    void OTAProviderSetup();
    void startOTAProvider();
};

#endif