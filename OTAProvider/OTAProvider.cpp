

#include "OTAProvider.h"

    OTAProvider::OTAProvider(char* ssidwifi, char* passwordwifi){
      ssidWifiOTA = ssidwifi;
      passwordWifiOTA = passwordwifi;
      pskArduinoOTA = "admin";
      port = 8266;
      hostName = "myArduino-esp8266";
    }

    OTAProvider::OTAProvider(char* ssidwifi, char* passwordwifi, char* parampskArduinoOTA){
      ssidWifiOTA = ssidwifi;
      passwordWifiOTA = passwordwifi;
      pskArduinoOTA = parampskArduinoOTA;
      port = 8266;
      hostName = "myArduino-esp8266";
    }

    OTAProvider::OTAProvider(char* ssidwifi, char* passwordwifi, char* parampskArduinoOTA,
                int paramPort, char* paramHostName){
      ssidWifiOTA = ssidwifi;
      passwordWifiOTA = passwordwifi;
      pskArduinoOTA = parampskArduinoOTA;
      port = paramPort;
      hostName = paramHostName;
    }  

    void OTAProvider::OTAProviderSetup(){
      Serial.begin(115200);
      Serial.println("Booting");
      WiFi.mode(WIFI_STA);
      WiFi.begin(ssidWifiOTA, passwordWifiOTA);
      int retries = 0;
      int MAX_WIFI_INIT_RETRY = 5;
      while ((WiFi.waitForConnectResult() != WL_CONNECTED) && (retries < MAX_WIFI_INIT_RETRY)){
        Serial.println("Connection Failed! Trying again...");
        delay(5000);
        retries++;
      }
      if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.println("Connec tion Failed! Rebooting...");
        ESP.restart();
      }
      // Port defaults to 8266
       ArduinoOTA.setPort(port);
    
      // Hostname defaults to esp8266-[ChipID]
       ArduinoOTA.setHostname(hostName);
        
      // No authentication by default
      ArduinoOTA.setPassword(pskArduinoOTA);
    
      ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH) {
          type = "sketch";
        } else { // U_SPIFFS
          type = "filesystem";
        }
    
        // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
        Serial.println("Start updating " + type);
      });
      ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
      });
      ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
      });
      ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
          Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
          Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
          Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
          Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
          Serial.println("End Failed");
        }
      });
      ArduinoOTA.begin();
      Serial.println("Ready");
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
    }

    void OTAProvider::startOTAProvider(){
      ArduinoOTA.handle();
     }
