#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <ArduinoJson.h>
#include <OTAProvider.h>
#include <ESP8266WebServer.h>


#define HTTP_REST_PORT 8080

ESP8266WebServer http_rest_server(HTTP_REST_PORT);

OTAProvider otaProvider("Fibertel WiFi685 2.4GHz","0049570753");

struct Led {
    byte id;
    byte gpio;
    byte status;
} led_resource;


//IR
const uint16_t kIrLed = 4;  // ESP8266 GPIO pin to use. Recommended: 4 (D2).

IRsend irsend(kIrLed);  // Set the GPIO to be used to sending the message.

//IRSend irSend;

void setupIR(){
   irsend.begin();
#if ESP8266
  Serial.begin(115200, SERIAL_8N1, SERIAL_TX_ONLY);
#else  // ESP8266
  Serial.begin(115200, SERIAL_8N1);
#endif  // ESP8266
}

void sendCommand() {

  // check for text from the PC
  // the PC sends a string containing "r,n,a,b,c,d,e,..." where r is how many times to repeat the command,
  // n is the number of durations, and a/b/c/d/e/... are the durations.
  // the durations are how long each mark and space period of an infrared command should last, in microseconds.
    String post_body = http_rest_server.arg("plain");


    Serial.print("sending command: ");
    
    const size_t capacity = JSON_ARRAY_SIZE(100) + JSON_OBJECT_SIZE(3) + 40;
    DynamicJsonDocument jsonBody(capacity);

    deserializeJson(jsonBody, post_body);

    JsonVariant jrepetitions = jsonBody["repetitions"];
    JsonArray jbuff = jsonBody["buff"];
    JsonVariant jfrequency = jsonBody["frequency"]; 
    JsonVariant jcount = jsonBody["count"];
    
    int repetitions = jrepetitions.as<unsigned int>();
    uint16_t frequency = jfrequency.as<uint16_t>();
    uint16_t count = jcount.as<uint16_t>();
    uint16_t buff[100];
    
    for(int i = 0; i < count; i++)
        buff[i] = jbuff[i].as<uint16_t>();

    Serial.print("repetions");
    Serial.println(repetitions);
    Serial.print("buffer");
    for(int i = 0; i < count; i++)
        Serial.print(buff[i]);
    Serial.print("length");
    Serial.println(count);
    Serial.print("frequency");
    Serial.println(frequency);

       
    for(int i = 0; i < repetitions; i++) {
      irsend.sendRaw(buff, count, frequency); //void sendRaw(uint16_t buf[], uint16_t len, uint16_t hz);
      delay(40);
    }

    delay(5000); //5 second delay between each signal burst
    
    http_rest_server.sendHeader("IrCommand", "/ir");
    http_rest_server.send(200);
}

//end IR

void json_to_resource(DynamicJsonDocument& jsonBody) {
    int id, gpio, status;

    id = jsonBody["id"];
    gpio = jsonBody["gpio"];
    status = jsonBody["status"];

    Serial.println(id);
    Serial.println(gpio);
    Serial.println(status);

    led_resource.id = id;
    led_resource.gpio = gpio;
    led_resource.status = status;
}

void init_led_resource()
{
    pinMode(LED_BUILTIN, OUTPUT);
    led_resource.id = 0;
    led_resource.gpio = 0;
    led_resource.status = LOW;
}

void get_leds() {
    StaticJsonDocument<200> jsonObj;
    
    if (led_resource.id == 0)
        http_rest_server.send(204);
    else {
        jsonObj["id"] = led_resource.id;
        jsonObj["gpio"] = led_resource.gpio;
        jsonObj["status"] = led_resource.status;
        String json;
        serializeJson(jsonObj, json);
        http_rest_server.send(200, "application/json", json);
    }
}


void post_put_leds() {
    String post_body = http_rest_server.arg("plain");
    Serial.println(post_body);
    
    const size_t capacity = JSON_OBJECT_SIZE(3) + 20;
    DynamicJsonDocument jsonBody(capacity);

    deserializeJson(jsonBody, post_body);
    
    Serial.print("HTTP Method: ");
    Serial.println(http_rest_server.method());
    
    if (!jsonBody["id"]) {
        Serial.println("error in parsin json body");
        http_rest_server.send(400);
    }
    else {   
        if (http_rest_server.method() == HTTP_POST) {
            if ((jsonBody["id"] != 0) && (jsonBody["id"] != led_resource.id)) {
                json_to_resource(jsonBody);
                http_rest_server.sendHeader("Location", "/leds/" + String(led_resource.id));
                http_rest_server.send(201);
            }
            else if (jsonBody["id"] == 0)
              http_rest_server.send(404);
            else if (jsonBody["id"] == led_resource.id)
              http_rest_server.send(409);
        }
        else if (http_rest_server.method() == HTTP_PUT) {
            if (jsonBody["id"] == led_resource.id) {
                json_to_resource(jsonBody);
                http_rest_server.sendHeader("Location", "/leds/" + String(led_resource.id));
                http_rest_server.send(200);
            }
            else
              http_rest_server.send(404);
        }
    }
}

void toggle() {

    String post_body = http_rest_server.arg("plain");
    
    const size_t capacity = JSON_OBJECT_SIZE(1) + 10;
    DynamicJsonDocument jsonBody(capacity);

    deserializeJson(jsonBody, post_body);

    bool ledOn = jsonBody["ledOn"];

    if (ledOn) {
      digitalWrite(LED_BUILTIN, LOW);
    } else {
      digitalWrite(LED_BUILTIN, HIGH);
    }

    http_rest_server.sendHeader("Location", "/leds/toggle");
    http_rest_server.send(200);
}


void config_rest_server_routing() {
    http_rest_server.on("/", HTTP_GET, []() {
        http_rest_server.send(200, "text/html",
            "Welcome to the ESP8266 REST Web Server");
    });
    http_rest_server.on("/leds", HTTP_GET, get_leds);
    http_rest_server.on("/leds", HTTP_POST, post_put_leds);
    http_rest_server.on("/leds", HTTP_PUT, post_put_leds);
    http_rest_server.on("/leds/toggle", HTTP_PUT, toggle);

    http_rest_server.on("/ir", HTTP_POST, sendCommand);
}

////
void setup() {
  init_led_resource();
  otaProvider.OTAProviderSetup();

  config_rest_server_routing();
  http_rest_server.begin();
  Serial.println("HTTP REST Server Started");
}


void loop() {
  otaProvider.startOTAProvider();
  http_rest_server.handleClient();
}
